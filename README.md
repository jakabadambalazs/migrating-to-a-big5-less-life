# Migrating to a BIG5-less Life
There are many reasons for which you might have ended up in this repository, one of them being your concern about your privacy. This repository is intended to be a useful resource for those who wish to change the ways they have been acting in the past in their digital life...


## How is the repository structured
There are two main parts to this repository: The guide and the journals. There is not a right order in which they should be read. Some might prefer first checking on how people are affected by these changes. Others might want to jump right in and walk through the guide first. I would assume, since the journey is long, that you will be doing a bit of both. 

### [The Guide](./docs/guide)
The guide is a sequential reading that will guide you through the steps that should considered when working out a strategy for the protection of your privacy. 

### [The Journals](./docs/journals)
The journals are a collection of personal experiences written in a journal format to document the process, the difficulties and the achievements.


## How to Contribute - Call for action
Before you read any further you need to know that this repository needs your contribution. It is very limited what one person can do but there is no limit in how much a community can achieve. There is an extreme number of combinations of hardware and software. Too numerous for one person to know of all of them. At the same time the environment we are talking about is highly dynamic with new components added to its complexity every day. This is why, if you feel you are able to contribute, then you should. Please refer to the documentation on [how to contribute](./docs/other/how_to_contribute.md).

