# How to Contribute

Your contribution is more than welcome. This is a community project and it should be built by the community. So please read below to check what might be the best way for you to contribute.

*Here are some simple contribution guidelines to keep(make?) the repo organized.*

## Project organization

* Branch `master` is always stable (once we get to that point).
* Branch `develop` is for development of the next version and merged into `master` when stable.


## Tools

### Issue Tracker
For one-off small contributions such as ideas and observations you can use the [Issue tracker](../../../../issues). You will also find the link for it in the left hand side menu called "Issues". Reporting of errors and any generic issues with what is described in the repository should also be reported through here. Another reason why you would want to use the Issue Tracker is because you do not know what is git (described below). If terms like "checkout" and "pull request" sound unfamiliar to you then you don't need to read further.

**Try not open a duplicate issue!**

1. Look through existing issues to see if your issue already exists.
2. If your issue already exists, comment on its thread with any information you have. Even if this is simply to note that you are having the same problem, it is still helpful!
3. Always *be as descriptive as you can*.
4. What is the expected behavior? What is the actual behavior? What are the steps to reproduce?
5. Attach screenshots, videos, GIFs if possible.
6. **Include library/application version information.**
7. **Include OS version and device model names.**


### Git 
If you are familiar with git then follow the steps below. 

1. Find an issue to work on, or create a new one.
2. Fork the repo, or make sure you are synced with the latest changes on `master`.
3. Create a new branch with a sweet name: `git checkout -b issue_<##>_<description>`.
4. Add your stuff by adhering to the guidelines below.
5. Create, update, double-check the links in the markdown files.
6. **Rebase on `master` branch and resolve any conflicts _before submitting a pull request!_**
7. Submit a pull request to the `master` branch.

**You should submit one pull request per feature!**

*The above instruction refer to the `master` branch only as long as we get to the first decent version of the guide. After that a `develop` branch will be created for the PRs.*

## Style guidelines

- You cannot go much wrong with the markdown files. Keep them neat and sensibly organized.
- Name files in lowercase with words separated by underscore.

## Content

For now, just be reasonable and remember that this guide is not for experts. So keep your language simple and explain and/or reference wherever it is required.
