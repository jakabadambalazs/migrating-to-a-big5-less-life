# Background Information
[Back to Index](./README.md)
____________________________

## What is privacy?
...

## Who are the BIG5?
The "big five" -- Apple, Alphabet, Microsoft, Facebook and Amazon -- now have a combined valuation of over $3.3 trillion, 
and make up more than 40 percent of the value of the Nasdaq 100 index (Sen, 2017).

- Amazon
- Apple
- Facebook
- Google
- Microsoft


## List of tools and services you might be using controlled by the BIG5?
The following is not a full list but it contains the most well known tools, apps or services.

- Amazon ([list1](https://en.wikipedia.org/wiki/List_of_Amazon_products_and_services))
    - Amazon.com and Amazon app
    - Amazon Web Services (AWS)
    - Amazon Drive
    - Amazon Fresh
    - Amazon Garage
    - Amazon Books
    - Amazon Kindle
    - Amazon Go
    - Amazon Video
- Apple
    - iOS
    - Apple Store
    - iCloud
    - iPad
    - iPhone
    - iWatch
    - Apple TV
    - Apple Music
    - iPod
    - iTunes
    - Apple Health
    - Siri
    - Apple pay
    - Apple Card
    - FaceID
    - TouchID 
    - Apple News+
- Facebook
    - Facebook.com and Facebook app
    - Facebook messenger
    - Whatsapp messenger
- Google(Alphabet) ([list1](https://en.wikipedia.org/wiki/List_of_Google_products)) ([list2](https://developers.google.com/products/))
    - Google.com search engine
    - Google Maps
    - GMail
    - Google Contacts
    - Google Drive
    - Google Docs
    - Google Calendar
    - Google Keep
    - Google Home
    - Google Fit
    - Google Hangouts
    - Youtube & Youtube Kids
    - Play Music
    - Google Photos
    - Google Translate
    - Google Play Services
    - any commercial Android powered phone
- Microsoft ([list1](https://en.wikipedia.org/wiki/List_of_Microsoft_software))
    - Microsoft Windows OS
    - Bing
    - GitHub
    - LinkedIn
    - Outlook.com
    - Microsoft Office365
    - OneDrive
    - Live Mail
    - Skype
    - Xbox
    - Money
    - Outlook
    - OneNote
    - Microsoft ToDo (used to be Wunderlist)
    - Microsoft Azure
    - Alexa
    - Solitaire
    

## Why are you (should you be) concerned?
...


## Where can I find more information about the subject?
- [GENERAL DATA PROTECTION REGULATION (GDPR)](/assets/GDPR_REGULATION_(EU)_2016_679.pdf) 2016/679 OF THE EUROPEAN 
PARLIAMENT AND OF THE COUNCIL - 27 April 2016 — On the protection of natural persons with regard to the processing of 
personal data and on the free movement of such data, and repealing Directive 95/ 46/ EC 
(General Data Protection Regulation).



___________________________
[Back to Index](./README.md)
___________________________
