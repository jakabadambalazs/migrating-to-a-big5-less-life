# The Plan
[Back to Index](./README.md)
____________________________

- Go to Google Takeout and have your backup prepared
- Install an alternative browser
- Choose and use an alternative search engine
- Set up a password Manager
- Set up an alternative mail account (and use it! === abandon GMail)
- Set up an alternative cloud file storage
- Create a Working Notes file to be able to share info across devices (keep it on Mega) 





## Do a Software Substitution Checklist
To start with these are the software components that you are suggested to substitute with an alternative. The suggested
alternatives might not always satisfy you 100%. Look for other ones and if you find a good one share it with us.


## Do an On-line Tool Substitution Checklist
There are some on-line tools that you continuously access through your devices.


#### Other Alternatives
[Google Alternatives](https://nomoregoogle.com/)







___________________________
[Back to Index](./README.md)
___________________________