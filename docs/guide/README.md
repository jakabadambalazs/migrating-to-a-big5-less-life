# Guide to a BIG5-less Life

This guide is intended to be read as a book. As such it should be read sequentially because the respective sections build on one another. This is however not to say that you cannot skip parts. You can, and once you understand that a specific part in not applicable or feasible for you, you should skip it. Having said this, this is not a programming guide in which if you copy and paste pieces of code mechanically the outcome will work. The parts are written in such a way that it should be possible to evaluate the impacts a certain change will have on you so that you can decide if it is worth for you to go ahead or not.

Off we go!


## Content

1. [Background Information](./docs/background_information.md)
2. [Preparation](./docs/preparation.md)
3. [The plan](./docs/plan.md)
4. The Absolutely Bare Minimum
5. ???
6. Other 
7. Future Considerations

