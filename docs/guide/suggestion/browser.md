# Browser
[Back to Index](../README.md)
____________________________

Your browser is your primary window on internet. You have three well known browsers from three of the BIG5: 
Google Chrome, Internet Explorer or Edge, Safari. You will want to start to look around for an alternative for those.

| Software Alternative                                                                  | Lnx | Mac | Win | And | iOS | 
| ------------------------------------------------------------------------------------- |:---:|:---:|:---:|:---:|:---:|
| [Brave](https://brave.com/)                                                           |  X  |  X  |  X  |  X  |  X  |
| [Mozilla Firefox](https://firefox.com/)                                               |  X  |  X  |  X  |  X  |  X  |
| [Tor](https://www.torproject.org/)                                                    |  X  |  X  |  X  |  X  |     |
| [Opera](https://www.opera.com/)                                                       |  X  |  X  |  X  |  X  |     |
| [Vivaldi](https://vivaldi.com/)                                                       |  X  |  X  |  X  |  β  |     |
| [Ungoogled Chromium](https://ungoogled-software.github.io/ungoogled-chromium-binaries)|  X  |  X  |  X  |     |     |

