# Preparation
[Back to Index](./README.md)
____________________________

In this section you will not be doing anything yet. Just a situation assessment.


## Make up your mind
This is about you. It is you who in the first place needs to make up his ming because the transition will not be easy
and some choices will cost you. They will cost you time and trouble, loss of comfort, reduced possibilities to interact 
with people (that you probably don't know) and at last but not least some choices might cost you money.
So this preparation section is really about you preparing yourself. You will be given the input of what it takes to
go ahead with this choice and by the end of the section you will be able to decide. 

You might also realize that even though you want to do something about your privacy, not all the things are possible 
for you to do. That is OK. Most people will in fact be in such a situation and only a few paranoid maniacs will
implement everything on the menu. So think for yourself and evaluate your situation and your possibilities.






___________________________
[Back to Index](./README.md)
___________________________